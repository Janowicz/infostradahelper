package main.java.exception;

public class PathNotFoundException extends Exception{

    public PathNotFoundException() {
        System.err.println("Path does not exists!");
    }
}
