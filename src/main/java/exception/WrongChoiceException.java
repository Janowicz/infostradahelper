package exception;

public class WrongChoiceException extends Exception{

    public WrongChoiceException() {
        System.err.println("Wrong choice!");
    }
}
