package main.java.exception;

public class CanNotReadPdfException extends Exception {
    public CanNotReadPdfException(String name) {
        System.err.println("Can not read " + name);
    }

    public CanNotReadPdfException() {
    }
}
