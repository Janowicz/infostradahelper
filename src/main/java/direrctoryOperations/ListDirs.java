package main.java.direrctoryOperations;

import main.java.exception.PathNotFoundException;
import main.java.io.ConsolePrinter;
import main.java.io.DataReader;

import java.io.File;
import java.io.IOException;

public class ListDirs {

    private DataReader dataReader = new DataReader();
    private ConsolePrinter consolePrinter = new ConsolePrinter();
    private int i = 1;

    public void listDirsToTxtFile() throws IOException, PathNotFoundException {

        consolePrinter.enterPathToCreateResultFile();
        File resultFilePath = new File(dataReader.loadCharacters());
        dataReader.createResultFile(resultFilePath);

        consolePrinter.enterPathToDirectories();
        File dirsPath = new File(dataReader.loadCharacters());
        dataReader.openBufferedWriter();
        dataReader.writeLineToTxtFile("Directory name\tPath");
        iterateDirectories(dirsPath);
        i = 1;
    }

    private void iterateDirectories(File directoryName ) throws IOException {
        for (File fileEntry : directoryName.listFiles()){
            if (fileEntry.isDirectory()){
                dataReader.writeLineToTxtFile(fileEntry.getName() + "\t" + fileEntry.getPath());
                iterateDirectories(fileEntry);
                System.out.println(i++ + " " + fileEntry.getName());
            }
        }
    }
}
