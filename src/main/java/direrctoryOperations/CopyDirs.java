package main.java.direrctoryOperations;

import main.java.exception.PathNotFoundException;
import main.java.io.ConsolePrinter;
import main.java.io.DataReader;
import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

public class CopyDirs {

    private ConsolePrinter consolePrinter = new ConsolePrinter();
    private DataReader dataReader = new DataReader();
    private String dataPathString;
    private String line;
    private File dataPathFile;
    private File sourceDirs;
    private File destDirs;
    private File errorPath;
    private String sourceData;
    private String destData;
    private Map<String, String> dirsMap = new HashMap();
    private Set<String> dirsMapKeySet;
    private int z = 1;

    public void copyDirsToAnother() throws IOException, PathNotFoundException {
        consolePrinter.printInfoInCopyDirsToAnother();
        consolePrinter.enterPathToTxtFileWithData();

        dataPathString = dataReader.loadCharacters();
        dataPathFile = new File(dataPathString);
        dataReader.checkIfFileExists(dataPathFile);
        addDataToMap();

        consolePrinter.enterPathToSourceDirectories();
        sourceDirs = new File(dataReader.loadCharacters());
        dataReader.checkIfFileExists(sourceDirs);

        consolePrinter.enterPathToDestinationDirectories();
        destDirs = new File(dataReader.loadCharacters());
        dataReader.checkIfFileExists(destDirs);

        consolePrinter.enterPathToCreateErrorFile();
        errorPath = new File(dataReader.loadCharacters());
        dataReader.createResultFile(errorPath);
        dataReader.openBufferedWriter();

        copyToDirs();
        z = 1;
    }

    private void copyToDirs() throws IOException {
        String[] sourceDirsList = sourceDirs.list();
        dirsMapKeySet = dirsMap.keySet();

        for (int i = 0; i < sourceDirsList.length; i++) {
            String dir = sourceDirsList[i];
            if (dirsMapKeySet.contains(dir)) {

                String sourceDirString = sourceDirs + File.separator + dir;
                File sourceDir = new File(sourceDirString);
                String value = dirsMap.get(dir);
                String fileDestinationString = destDirs + File.separator + value + File.separator + dir;
                File fileDestination1 = new File(fileDestinationString);

                String[] sourceDirFiles = sourceDir.list();

                if (!fileDestination1.exists()) {
                    fileDestination1.mkdir();

                    for (i = 0; i < sourceDirFiles.length; i++) {
                        System.out.println(sourceDirFiles[i]);

                        String newSourceFileString = sourceDirString + File.separator + sourceDirFiles[i];
                        File newSourceFile = new File(newSourceFileString);

//                        FileUtils.copyFileToDirectory(newSourceFile, fileDestination1);
                        Files.copy(newSourceFile.toPath(), fileDestination1.toPath());
                        System.out.println(z++ + " " + dir + " " + sourceDirFiles[i]);
                    }
                } else {
                    dataReader.writeLineToTxtFile("Doesn't copied directory: " + dir);
                }
            }
        }
    }

    private void addDataToMap() throws IOException {
        BufferedReader reader = dataReader.openFileBufferedReader(dataPathFile);
        line = reader.readLine();
        while (line != null){
            StringTokenizer tokenizer = new StringTokenizer(line, "|");
            sourceData = tokenizer.nextToken();
            destData = tokenizer.nextToken();
            dirsMap.put(sourceData, destData);
            line = reader.readLine();
        }
        dataReader.closeBufferedReader();
    }
}
