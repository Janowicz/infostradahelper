package main.java.pdf;

import main.java.io.DataReader;
import main.java.exception.PathNotFoundException;
import main.java.io.ConsolePrinter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

import java.io.*;
import java.util.Calendar;

public class Pdf {

    private ConsolePrinter consolePrinter = new ConsolePrinter();
    private DataReader dataReader = new DataReader();
    private File resultFile;
    private int i = 1;
    private File pathToDirectoriesFile;
    private final String SMALL_PDF_EXTENSION = ".pdf";
    private final String LARGE_PDF_EXTENSION = ".PDF";
    private String partText;
    private final static Double A10_WIDTH = 2.6;
    private final static Double A9_WIDTH = 3.7;
    private final static Double A8_WIDTH = 5.2;
    private final static Double A7_WIDTH = 7.4;
    private final static Double A6_WIDTH = 10.5;
    private final static Double A5_WIDTH = 14.8;
    private final static Double A4_WIDTH = 21.0;
    private final static Double A3_WIDTH = 29.7;
    private final static Double A2_WIDTH = 40.2;
    private final static Double A1_WIDTH = 59.4;
    private final static Double A0_WIDTH = 84.1;
    private final static Double _2A0_WIDTH = 118.9;
    private final static Double _4A0_WIDTH = 168.2;
    private final static Double A10_HEIGHT = 3.7;
    private final static Double A9_HEIGHT = 5.2;
    private final static Double A8_HEIGHT = 7.4;
    private final static Double A7_HEIGHT = 10.5;
    private final static Double A6_HEIGHT = 14.8;
    private final static Double A5_HEIGHT = 21.0;
    private final static Double A4_HEIGHT = 29.7;
    private final static Double A3_HEIGHT = 42.0;
    private final static Double A2_HEIGHT = 59.4;
    private final static Double A1_HEIGHT = 84.1;
    private final static Double A0_HEIGHT = 118.9;
    private final static Double _2A0_HEIGHT = 168.2;
    private final static Double _4A0_HEIGHT = 237.8;
    private final static String A10 = "A10";
    private final static String A9 = "A9";
    private final static String A8 = "A8";
    private final static String A7 = "A7";
    private final static String A6 = "A6";
    private final static String A5 = "A5";
    private final static String A4 = "A4";
    private final static String A3 = "A3";
    private final static String A2 = "A2";
    private final static String A1 = "A1";
    private final static String A0 = "A0";
    private final static String _2A0 = "2A0";
    private final static String _4A0 = "4A0";
    private final static String OVER_4A0 = "Oven than 4A0";

    public void addAttributes() throws IOException {
//        try {
//            final PDDocument document = PDDocument.load(new File("C:\\Users\\mjanowicz\\Desktop\\test\\test1\\dest2\\123.pdf"));
////            document = PDDocument.load(new File("C:\\Users\\mjanowicz\\Desktop\\test\\test1\\dest2\\1.pdf"));
//            PDDocumentInformation info = new PDDocumentInformation();
//            info.setTitle("asd");
//            info.setSubject("asd PDFBox adding meta-data to PDF document");
//            info.setAuthor("ger.com");
//            info.setCreator("gfd.com");
//            info.setProducer("Memorynotfound.com");
//            info.setKeywords("Apache, PdfBox, XMP, PDF");
//            info.setCreationDate(Calendar.getInstance());
//            info.setModificationDate(Calendar.getInstance());
//            info.setTrapped("Unknown");
//            info.setCustomMetadataValue("swag", "yes");
//
//            document.setDocumentInformation(info);
////            document.setVersion(1.5f);
//            document.save(new File("C:\\Users\\mjanowicz\\Desktop\\test\\test1\\dest2\\123.pdf"));
//        } catch (IOException e){
//            System.err.println("Exception while trying to create pdf document - " + e);
//        }
        consolePrinter.enterPathToPdfFile();
        try {
        String pdfFileString = dataReader.loadCharacters();
        File pdfFile = new File(pdfFileString);
        PDDocumentInformation information = new PDDocumentInformation();

        final PDDocument document = PDDocument.load(new File(pdfFileString));

        consolePrinter.setTitle();
        String setTitle = dataReader.loadCharacters();
        information.setTitle(setTitle);

        consolePrinter.setAuthor();
        String setAuthor = dataReader.loadCharacters();
        information.setAuthor(setAuthor);

        consolePrinter.setSubject();
        String setSubject = dataReader.loadCharacters();
        information.setSubject(setSubject);

        consolePrinter.setKeywords();
        String setKeywords = dataReader.loadCharacters();
        information.setKeywords(setKeywords);

        document.setDocumentInformation(information);
//        document.close();
        document.save(new File(pdfFileString));
//        document.close();
        System.out.println("Attributes was added " + pdfFileString);
        } catch (Exception e) {
            dataReader.writeLineToTxtFile("Can not add attributes");
            System.err.println("Can not add attributes");
        }
    }

    public void getPageSizeCase4() throws IOException, PathNotFoundException {
        File createTxtFileAndSetPathToIterate = createFileAndSetPathToIterate();
        dataReader.writeLineToTxtFile("File name\tNumber page\tWidth\tLength\tFormat\nPath");
        i = 1;
        filesInDirectoriesCase4(createTxtFileAndSetPathToIterate);
        dataReader.closeBufferedWriter();
    }

    private void filesInDirectoriesCase4(File directoryName) throws IOException {
        for (File fileEntry : directoryName.listFiles()){
            if (fileEntry.isDirectory()){
                filesInDirectoriesCase4(fileEntry);
            } else {
                if (fileEntry.getName().endsWith(SMALL_PDF_EXTENSION) ||
                        fileEntry.getName().endsWith(LARGE_PDF_EXTENSION)){
                    PDDocument document = null;
                    float widthCM;
                    float heightCM;
                    try {
                        document = PDDocument.load(new File(fileEntry.getPath()));
                    } catch (Exception e) {
                        dataReader.writeLineToTxtFile("Can not read: " + fileEntry.getName() + "\t"
                                + fileEntry.getPath());
                    }

                    int numberOfPages = document.getNumberOfPages();

                    for (int j = 0; j < numberOfPages; j++) {
                        float widthPt = document.getPage(j).getMediaBox().getWidth();
                        float heightPt = document.getPage(j).getMediaBox().getHeight();

                        widthCM = ptToCm(widthPt);
                        heightCM = ptToCm(heightPt);

                        if (widthCM > heightCM){
                            float temporaryVariable = widthCM;
                            widthCM = heightCM;
                            widthCM *= 100;
                            widthCM = Math.round(widthCM);
                            widthCM /= 100;
                            heightCM = temporaryVariable;
                            heightCM *= 100;
                            heightCM = Math.round(heightCM);
                            heightCM /= 100;
                        } else {
                            widthCM *= 100;
                            widthCM = Math.round(widthCM);
                            widthCM /= 100;
                            heightCM *= 100;
                            heightCM = Math.round(heightCM);
                            heightCM /= 100;
                        }

                        String format = getFormat(widthCM, heightCM);
                        dataReader.writeLineToTxtFile(fileEntry.getName() + "\t" + ++j + "\t" + widthCM + "\t" +
                                heightCM + "\t" + format + "\t" + fileEntry.getPath());
                        System.out.println(i++ + " " + fileEntry.getName());
                        j--;

                    }
                    document.close();
                }
            }
        }
    }

    private String getFormat(float widthPage, float heightPage){
        if (heightPage <= A10_HEIGHT && widthPage <= A10_WIDTH){
            return A10;
        } else if (heightPage <= A10_HEIGHT && widthPage <= A10_HEIGHT){
            return A9;
        } else if (heightPage <= A9_HEIGHT && widthPage <= A9_WIDTH){
            return A9;
        } else if (heightPage <= A9_HEIGHT && widthPage <= A9_HEIGHT) {
            return A8;
        } else if (heightPage <= A8_HEIGHT && widthPage <= A8_WIDTH){
            return A8;
        } else if (heightPage <= A8_HEIGHT && widthPage <= A8_HEIGHT) {
            return A7;
        } else if (heightPage <= A7_HEIGHT && widthPage <= A7_WIDTH){
            return A7;
        } else if (heightPage <= A7_HEIGHT && widthPage <= A7_HEIGHT) {
            return A6;
        } else if (heightPage <= A6_HEIGHT && widthPage <= A6_WIDTH){
            return A6;
        } else if (heightPage <= A6_HEIGHT && widthPage <= A6_HEIGHT) {
            return A5;
        } else if (heightPage <= A5_HEIGHT && widthPage <= A5_WIDTH){
            return A5;
        } else if (heightPage <= A5_HEIGHT && widthPage <= A5_HEIGHT) {
            return A4;
        } else if (heightPage <= A4_HEIGHT && widthPage <= A4_WIDTH){
            return A4;
        } else if (heightPage <= A4_HEIGHT && widthPage <= A4_HEIGHT) {
            return A3;
        } else if (heightPage <= A3_HEIGHT && widthPage <= A3_WIDTH){
            return A3;
        } else if (heightPage <= A3_HEIGHT && widthPage <= A3_HEIGHT) {
            return A2;
        } else if (heightPage <= A2_HEIGHT && widthPage <= A2_WIDTH){
            return A2;
        } else if (heightPage <= A2_HEIGHT && widthPage <= A2_HEIGHT) {
            return A1;
        } else if (heightPage <= A1_HEIGHT && widthPage <= A1_WIDTH){
            return A1;
        } else if (heightPage <= A1_HEIGHT && widthPage <= A1_HEIGHT) {
            return A0;
        } else if (heightPage <= A0_HEIGHT && widthPage <= A0_WIDTH){
            return A0;
        } else if (heightPage <= A0_HEIGHT && widthPage <= A0_HEIGHT) {
            return _2A0;
        } else if (heightPage <= _2A0_HEIGHT && widthPage <= _2A0_WIDTH){
            return _2A0;
        } else if (heightPage <= _2A0_HEIGHT && widthPage <= _2A0_HEIGHT) {
            return _4A0;
        } else if (heightPage <= _4A0_HEIGHT && widthPage <= _4A0_WIDTH){
            return _4A0;
        } else {
            return OVER_4A0;
        }
    }

    public void countPagesByTextCase3() throws IOException, PathNotFoundException {
        consolePrinter.enterPartTextPDFContains();
        partText = dataReader.loadCharacters();
        File createTxtFileAndSetPathToIterate = createFileAndSetPathToIterate();
        dataReader.writeLineToTxtFile("File name\tNumber of pages\tPath");
        filesInDirectoriesCase3(createTxtFileAndSetPathToIterate);
        dataReader.closeBufferedWriter();
        i = 1;
    }

    private void filesInDirectoriesCase3(File directoryName) throws IOException {
        for (File fileEntry : directoryName.listFiles()){
            if (fileEntry.isDirectory()){
                filesInDirectoriesCase3(fileEntry);
            } else {
                if (fileEntry.getName().endsWith(SMALL_PDF_EXTENSION) ||
                        fileEntry.getName().endsWith(LARGE_PDF_EXTENSION)) {
                    String temporaryFileName = fileEntry.getName().toLowerCase();
                    String temporaryPartText = partText.toLowerCase();
                    if (temporaryFileName.contains(temporaryPartText)){
                        countPagesInPdf(fileEntry);
                        System.out.println(i++ + " " + temporaryFileName);
                    }
                }
            }
        }
    }

    public void countAllPagesCase2() throws IOException, PathNotFoundException {
        File createTxtFileAndSetPathToIterate = createFileAndSetPathToIterate();
        dataReader.writeLineToTxtFile("File name\tNumber of pages\tPath");
        filesInDirectoriesCase2(createTxtFileAndSetPathToIterate);
        dataReader.closeBufferedWriter();
        i = 1;
    }

    private void filesInDirectoriesCase2(File directoryName) throws IOException {
        for (File fileEntry : directoryName.listFiles()) {
            if (fileEntry.isDirectory()) {
                filesInDirectoriesCase2(fileEntry);
            } else {
                if (fileEntry.getName().endsWith(SMALL_PDF_EXTENSION) ||
                        fileEntry.getName().endsWith(LARGE_PDF_EXTENSION)) {
                    System.out.println(i++ + " " + fileEntry.getName());
                    countPagesInPdf(fileEntry);
                }
            }
        }
    }

    public void defectivePdfCase1() throws IOException, PathNotFoundException {

        File createTxtFileAndSetPathToIterate = createFileAndSetPathToIterate();
        filesInDirectoriesCase1(createTxtFileAndSetPathToIterate);
        dataReader.closeBufferedWriter();
        i = 1;
    }

    private void filesInDirectoriesCase1(File directoryName) throws IOException {
        for (File fileEntry : directoryName.listFiles()) {
            if (fileEntry.isDirectory()) {
                filesInDirectoriesCase1(fileEntry);
            } else {
                if (fileEntry.getName().endsWith(SMALL_PDF_EXTENSION) ||
                        fileEntry.getName().endsWith(LARGE_PDF_EXTENSION)) {
                    System.out.println(i++ + " " + fileEntry.getName());
                    try {
                        PDDocument document = PDDocument.load(new File(fileEntry.getPath()));
                        document.close();
                    } catch (Exception e) {
                        dataReader.writeLineToTxtFile("Can not read: " + fileEntry.getName() + "\t"
                                + fileEntry.getPath());
                    }
                }
            }
        }
    }

    private void countPagesInPdf(File fileEntry) throws IOException {
        try {
            PDDocument document = PDDocument.load(new File(fileEntry.getPath()));
            int numberOfPages = document.getNumberOfPages();
            dataReader.writeLineToTxtFile(fileEntry.getName() + "\t"
                    + numberOfPages + "\t" + fileEntry.getPath());
            document.close();
        } catch (Exception e) {
            dataReader.writeLineToTxtFile("Can not read: " + fileEntry.getName() + "\t"
                    + fileEntry.getPath());
        }
    }

    private File createFileAndSetPathToIterate() throws IOException, PathNotFoundException {
        consolePrinter.enterPathToCreateResultFile();
        File pathToNewTxtFile = new File(dataReader.loadCharacters());

        resultFile = dataReader.createResultFile(pathToNewTxtFile);

        consolePrinter.enterPathToDirectories();
        String pathToDirectoriesString = dataReader.loadCharacters();
        pathToDirectoriesFile = new File(pathToDirectoriesString);
        dataReader.openBufferedWriter();

        return pathToDirectoriesFile;
    }

    public float ptToCm(float pt) {
        return (float) (2.54 * pt / 72);
    }
}


