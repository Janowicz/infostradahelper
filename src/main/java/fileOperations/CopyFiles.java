package main.java.fileOperations;

import main.java.app.HelperControl;
import main.java.exception.PathNotFoundException;
import main.java.io.ConsolePrinter;
import main.java.io.DataReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.*;

public class CopyFiles {

    private ConsolePrinter consolePrinter = new ConsolePrinter();
    private DataReader dataReader = new DataReader();
    private Set<String> setStringList = new HashSet<>();
    private Map<String, File> filesMap = new HashMap<>();
    private File sourceFile;
    private int i = 1;
    private File filesPath;
    private File dirsPath;
    private String localName;
    private String newLocalName;
    private File[] files;
    private List<String> filesList = new ArrayList<>();
    private File destDir;
    private File sourceDir;

    public void copyFilesByTxtFile() throws IOException, PathNotFoundException {
        consolePrinter.enterPAthToTxtFileWithNamesFiles();
        File txtFile = new File(dataReader.loadCharacters());
        dataReader.checkIfFileExists(txtFile);

        consolePrinter.enterPathFromWillBeCopiedFiles();
        sourceDir = new File(dataReader.loadCharacters());
        dataReader.checkIfFileExists(sourceDir);

        consolePrinter.enterDestinationPath();
        destDir = new File(dataReader.loadCharacters());
        dataReader.checkIfFileExists(destDir);

        BufferedReader reader = dataReader.openFileBufferedReader(txtFile);
        String line = reader.readLine();
        while (line != null) {
            filesList.add(line);
            line = reader.readLine();
        }
        dataReader.closeBufferedReader();

        copyFilesByTxtFileIterate(sourceDir);

        System.out.println("Don't copied: ");
        Iterator iterator = filesList.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }

    private void copyFilesByTxtFileIterate(File source) throws IOException {
        for (File fileEntry : source.listFiles()) {
            if (fileEntry.isDirectory()) {
                copyFilesByTxtFileIterate(fileEntry);
            } else {
                if (fileEntry.isFile()) {
                    if (filesList.contains(fileEntry.getName())) {
                        File destinationFile = new File(destDir + File.separator + fileEntry.getName());
                        Files.copy(fileEntry.toPath(), destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                        System.out.println(i + " Copied file: " + fileEntry.getName());
                        filesList.remove(fileEntry.getName());
                        i++;
                    }
                }
            }
        }
    }


    public void copyFilesIfTifHasTheSameName() throws IOException, PathNotFoundException {
        enterPathToDirs();
        File[] filesList1 = filesPath.listFiles();

        for (int j = 0; j < filesList1.length; j++) {
            localName = filesList1[j].getName();
            setStringList.add(localName);
        }
        copyFilesIfTifHasTheSameNameIterate(dirsPath);
        setStringList.clear();
        i = 1;
    }

    private void copyFilesIfTifHasTheSameNameIterate(File directoryName) throws IOException {
        for (final File fileEntry : directoryName.listFiles()) {
            if (fileEntry.isDirectory()) {
                copyFilesIfTifHasTheSameNameIterate(fileEntry);
            } else {
                if (fileEntry.isFile() &&
                        (fileEntry.getName().contains(".tif") || fileEntry.getName().contains(".TIF"))) {

                    Iterator<String> iterator = setStringList.iterator();
                    while (iterator.hasNext()) {
                        String fileName = iterator.next();
                        int nameWithoutExtensionPosition = fileName.lastIndexOf(".");
                        String localName2 = fileName.substring(0, nameWithoutExtensionPosition);
                        String tifName = fileEntry.getName().substring(0, (fileEntry.getName().length()) - 4);
                        if (tifName.equals(localName2)) {
                            File from = new File(filesPath + File.separator + fileName);
                            File to = new File(fileEntry.getParent() + File.separator + fileName);
                            Files.copy(from.toPath(), to.toPath(), StandardCopyOption.REPLACE_EXISTING);
                            System.out.println("Copied: " + fileName);
                        }
                    }
                }
            }
        }
    }

    public void copyFilesFromOneDirToAnother(String variant) throws IOException, PathNotFoundException {

        switch (variant) {
            case HelperControl.COPY_FILES_TO_DIRS_BY_CONTAINS_NAME:
                consolePrinter.printInfoInCopyFilesToAnotherDirsByContainsName();
                enterPathToDirs();
                File[] filesList1 = filesPath.listFiles();

                for (int j = 0; j < filesList1.length; j++) {
                    localName = filesList1[j].getName();
                    setStringList.add(localName);
                }
                iterateDirContainsFileName(dirsPath);
                setStringList.clear();
                break;
            case HelperControl.COPY_FILES_TO_DIRS_BEFORE__:
                consolePrinter.printInfoInCopyFilesToAnotherDirsByBefore__();
                enterPathToDirs();

                File[] filesList2 = filesPath.listFiles();

                for (int k = 0; k < filesList2.length; k++) {
                    localName = filesList2[k].getName();
                    if (localName.contains("_")) {
                        filesMap.put(localName, filesList2[k]);
                    }
                }
                setStringList = filesMap.keySet();
                iterateDirEqualsFileName(dirsPath);
                setStringList.clear();
                break;
        }


    }

    private void iterateDirEqualsFileName(File directoryName) throws IOException {

        for (File fileEntry : directoryName.listFiles()) {
            if (fileEntry.isDirectory()) {
                String fileEntryName = fileEntry.getName();
                Iterator<String> iterator = setStringList.iterator();

                while (iterator.hasNext()) {
                    String file = iterator.next();
                    int _Position = file.indexOf("_");
                    String dir = file.substring(0, _Position);
                    if (fileEntryName.equals(dir)) {
                        File from = filesMap.get(file);
                        File to = new File(fileEntry + File.separator + file);
                        Files.copy(from.toPath(), to.toPath(), StandardCopyOption.REPLACE_EXISTING);
                        System.out.println("Copied: " + file);
                    }
                }
                iterateDirEqualsFileName(fileEntry);
            }
        }
    }


    public void iterateDirContainsFileName(File directoryName) throws IOException {

        for (File fileEntry : directoryName.listFiles()) {
            if (fileEntry.isDirectory()) {
                String fileEntryName = fileEntry.getName();
                Iterator<String> iterator = setStringList.iterator();
                while (iterator.hasNext()) {
                    String next = iterator.next();
                    File localFile = new File(filesPath + File.separator + next);
                    if (next.contains(fileEntryName)) {
                        File destFile = new File(fileEntry + File.separator + next);

                        Path from = localFile.toPath();
                        Path to = destFile.toPath();

                        Files.copy(from, to, StandardCopyOption.REPLACE_EXISTING);
                        System.out.println("Copied: " + destFile);

                    }
                }
                iterateDirContainsFileName(fileEntry);
            }
        }
        i = 1;
    }

    private void enterPathToDirs() throws IOException, PathNotFoundException {
        consolePrinter.enterPathToDirectoryWithFiles();
        filesPath = new File(dataReader.loadCharacters());
        dataReader.checkIfFileExists(filesPath);

        consolePrinter.enterPathToDirectories();
        dirsPath = new File(dataReader.loadCharacters());
        dataReader.checkIfFileExists(dirsPath);
    }
}
