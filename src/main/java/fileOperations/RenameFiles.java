package main.java.fileOperations;

import main.java.exception.PathNotFoundException;
import main.java.io.ConsolePrinter;
import main.java.io.DataReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

public class RenameFiles {

    private ConsolePrinter consolePrinter = new ConsolePrinter();
    private DataReader dataReader = new DataReader();
    private int i = 1;

    public void renameFilesAdd_AndDirName() throws IOException {
        consolePrinter.printInfoRenameFilesToAdd_AndDirName();
        consolePrinter.enterPathToDirectories();
        File dirsPath = new File(dataReader.loadCharacters());
        renameFilesAdd_AndDirNameIterate(dirsPath);
        i = 1;
    }

    private void renameFilesAdd_AndDirNameIterate(File directoryName){
        for (File fileEntry : directoryName.listFiles()){
            if (fileEntry.isDirectory()){
                renameFilesAdd_AndDirNameIterate(fileEntry);
            } else {
                if (fileEntry.isFile()) {
                    String parentDirPath = fileEntry.getParent();
                    String newParentDirPath = parentDirPath.replace(File.separator, "|");
                    String[] splitPath = newParentDirPath.split("\\|");
                    int arrayLength = splitPath.length;
                    String parentDir = splitPath[arrayLength - 1];
                    if (!fileEntry.getName().startsWith(parentDir)){
                        String newName = parentDirPath + File.separator +
                                parentDir + "_" + fileEntry.getName().trim();
                        fileEntry.renameTo(new File(newName));
                        System.out.println(i++ + " Renamed: " + newName);
                    }
                }
            }
        }
    }


    public void renameXml() throws IOException, PathNotFoundException {
        consolePrinter.renameXml();
        consolePrinter.enterPathToDirectoryWithFiles();
        File xmlPath = new File(dataReader.loadCharacters());
        dataReader.checkIfFileExists(xmlPath);

        iteratoToRemaneXml(xmlPath);
        i = 1;
    }

    private void iteratoToRemaneXml(File directoryName) throws IOException {
        for (File fileEntry : directoryName.listFiles()){
            if (fileEntry.isDirectory()){
                iteratoToRemaneXml(fileEntry);
            } else {
                if (fileEntry.isFile() &&
                        (fileEntry.getName().endsWith(".xml") || fileEntry.getName().endsWith(".XML"))) {
                    BufferedReader reader = dataReader.openFileBufferedReader(fileEntry);
                    String line = reader.readLine();
                    String el1 = "";
                    String el2 = "";
                    String el3 = "";
                    String el4 = "";
                    String fullName = "";
                    while (line != null) {
                        if (line.contains("<pierwszyCzlon>")) {
                            int start = line.indexOf("<pierwszyCzlon>") + 15;
                            int end = line.indexOf("</pierwszyCzlon>");
                            el1 = line.substring(start, end);
                        } else if (line.contains("<drugiCzlon>")) {
                            int start = line.indexOf("<drugiCzlon>") + 12;
                            int end = line.indexOf("</drugiCzlon>");
                            el2 = line.substring(start, end);
                        } else if (line.contains("<trzeciCzlon>")) {
                            int start = line.indexOf("<trzeciCzlon>") + 13;
                            int end = line.indexOf("</trzeciCzlon>");
                            el3 = line.substring(start, end);
                        } else if (line.contains("<czwartyCzlon>")) {
                            int start = line.indexOf("<czwartyCzlon>") + 14;
                            int end = line.indexOf("</czwartyCzlon>");
                            el4 = line.substring(start, end);
                        }
                        fullName = el1 + "." + el2 + "." + el3 + "." + el4;
                        line = reader.readLine();
                    }
                    reader.close();
                    if (el1.equals("") || el2.equals("") || el3.equals("") || el4.equals("")){
                        System.out.println("One of index is empty " + fileEntry.getName());
                    } else {
                        File newFile = new File(fileEntry.getParent() + File.separator + fullName + ".xml");
                        if (newFile.exists()) {
                            System.out.println("This name is already exists " + fullName);
                        } else {
                            fileEntry.renameTo(newFile);
                            System.out.println(i++ + " Renamed: " + newFile.getName());
                        }
                    }
                }
            }
        }
    }
}
