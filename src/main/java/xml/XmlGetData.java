package main.java.xml;

import main.java.exception.PathNotFoundException;
import main.java.io.DataReader;
import main.java.io.ConsolePrinter;

import java.io.*;

public class XmlGetData {

    DataReader dataReader = new DataReader();
    ConsolePrinter consolePrinter = new ConsolePrinter();
    private File resultFile;
    private File pathToDirectoriesFile;
    private static final String obreb = "<obreb>";
    private static final String _obreb = "</obreb>";
    private static final String oznMaterialuZasobu = "<pzg_oznMaterialuZasobu>";
    private static final String _oznMaterialuZasobu = "</pzg_oznMaterialuZasobu>";
    private static final String pzg_opis = "<pzg_opis>";
    private static final String _pzg_opis = "</pzg_opis>";
    private static final String pzg_cel = "<pzg_cel>";
    private static final String _pzg_cel = "</pzg_cel>";
    private static final String celArchiwalny = "<celArchiwalny>";
    private static final String _celArchiwalny = "</celArchiwalny>";
    private String partText;
    private int partTextNumber = 0;
    private int numberOfFile = 1;
    private BufferedReader xmlReader;

    public void getDataFromFileFileByEnterText() throws IOException, PathNotFoundException {
        File fileAndSetPathToIterate = createFileAndSetPathToIterate();
        consolePrinter.enterPartTextFileContains();
        partText = dataReader.loadCharacters();
        dataReader.writeLineToTxtFile("File name\tValue\tPath");
        filesInDirectoriesCase6(fileAndSetPathToIterate);
        dataReader.closeBufferedWriter();
        numberOfFile = 1;
    }

    private void filesInDirectoriesCase6(File directoryName) throws IOException {
        for (File fileEntry : directoryName.listFiles()) {
            if (fileEntry.isDirectory()) {
                filesInDirectoriesCase6(fileEntry);
            } else if (fileEntry.isFile() && !fileEntry.getName().equals("result.txt")) {
                xmlReader = dataReader.openFileBufferedReader(fileEntry);
                String line = xmlReader.readLine();

                dataReader.writeTextToTxtFileWithoutPrintLine(fileEntry.getName());
                while (line != null) {
                    if (line.contains(partText)) {
                        if (partTextNumber == 0){
                            dataReader.writeTextToTxtFileWithoutPrintLine("\t" + line.trim());
                        } else {
                            dataReader.writeTextToTxtFileWithoutPrintLine("|" + line.trim());
                        }
                        partTextNumber++;
                    }
                    line = xmlReader.readLine();
                }
                dataReader.writeLineToTxtFile("\t" + fileEntry.getPath());
                dataReader.closeBufferedReader();
                System.out.println(numberOfFile++ + " " + fileEntry.getName());
            }
            partTextNumber = 0;
        }
    }


    public void getDataFromXml() throws IOException, PathNotFoundException {
        File fileAndSetPathToIterate = createFileAndSetPathToIterate();

        dataReader.writeLineToTxtFile("File name\tobęb\tpzg_opis\tKERG\tpzg_cel\tcelArchiwalny");
        filesInDirectoriesCase5(fileAndSetPathToIterate);
        dataReader.closeBufferedWriter();
        numberOfFile = 1;
    }


    private void filesInDirectoriesCase5(File directoryName) throws IOException {

        for (File fileEntry : directoryName.listFiles()) {
            if (fileEntry.isDirectory()) {
                filesInDirectoriesCase5(fileEntry);
            } else {
                if (fileEntry.getName().endsWith(".xml") || fileEntry.getName().endsWith(".XML")) {

                    xmlReader = dataReader.openFileBufferedReader(fileEntry);
                    String line = xmlReader.readLine();

                    int xmlIntName = fileEntry.getName().length() - 4;
                    String xmlStringName = fileEntry.getName().substring(0, xmlIntName);

                    dataReader.writeTextToTxtFileWithoutPrintLine(xmlStringName);

                    String indexStringObreb = "";
                    String indexStringoznMaterialuZasobu = "";
                    String indexStringPzg_cel = "";
                    String indexStringCelArchiwalny = "";
                    String indexPzg_opis = "";
                    int pzg_celNumber = 0;
                    int celArchiwalnyNumber = 0;
                    int obrebNumber = 0;
                    int lineNumber = 0;

                    while (line != null) {
                        if (line.contains(obreb)) {
                            int startIndexObreb = line.indexOf(obreb) + 7;
                            int endIndexObreb = line.indexOf(_obreb);

                            indexStringObreb = line.substring(startIndexObreb, endIndexObreb);
                            if (obrebNumber == 0 ){
                                dataReader.writeTextToTxtFileWithoutPrintLine("\t" + indexStringObreb);
                            } else if (obrebNumber > 0 && !(lineNumber > 50)){
                                dataReader.writeTextToTxtFileWithoutPrintLine("|" + indexStringObreb);
                            }
                            obrebNumber++;

                        } else if (line.contains(pzg_opis)){
                            int startIndexPzg_opis = line.indexOf(pzg_opis) + 10;
                            int endIndexPzg_opis = line.indexOf(_pzg_opis);

                            indexPzg_opis = line.substring(startIndexPzg_opis, endIndexPzg_opis);

                            dataReader.writeTextToTxtFileWithoutPrintLine("\t" + indexPzg_opis);
                        } else if (line.contains(oznMaterialuZasobu)) {
                            int startIndexoznMaterialuZasobu = line.indexOf(oznMaterialuZasobu) + 24;
                            int endIndexoznMaterialuZasobu = line.indexOf(_oznMaterialuZasobu);

                            indexStringoznMaterialuZasobu = line.substring(startIndexoznMaterialuZasobu, endIndexoznMaterialuZasobu);

                            dataReader.writeTextToTxtFileWithoutPrintLine("\t" + indexStringoznMaterialuZasobu);
                        } else if (line.contains(pzg_cel)) {
                            int startIndexPzg_cel = line.indexOf(pzg_cel) + 9;
                            int endIndexPzg_cel = line.indexOf(_pzg_cel);

                            indexStringPzg_cel = line.substring(startIndexPzg_cel, endIndexPzg_cel);

                            if (pzg_celNumber == 0){
                                dataReader.writeTextToTxtFileWithoutPrintLine("\t" + indexStringPzg_cel);
                            } else if (pzg_celNumber > 0 && !(lineNumber > 50)){
                                dataReader.writeTextToTxtFileWithoutPrintLine("|" + indexStringPzg_cel);
                            }
                            pzg_celNumber++;
                        } else if (line.contains(celArchiwalny)){
                            int startIndexCelArchiwalny = line.indexOf(celArchiwalny) + 15;
                            int endIndexCelArchiwalny = line.indexOf(_celArchiwalny);

                            indexStringCelArchiwalny = line.substring(startIndexCelArchiwalny, endIndexCelArchiwalny);

                            if (celArchiwalnyNumber == 0){
                                dataReader.writeTextToTxtFileWithoutPrintLine("\t" + indexStringCelArchiwalny);
                            } else if (celArchiwalnyNumber > 0 && !(lineNumber > 50)){
                                dataReader.writeTextToTxtFileWithoutPrintLine("|" + indexStringCelArchiwalny);
                            }
                            celArchiwalnyNumber++;
                        }
                        lineNumber++;
                        line = xmlReader.readLine();
                    }
                    dataReader.writeLineToTxtFile("");
                    dataReader.closeBufferedReader();
                    System.out.println(numberOfFile + " " + xmlStringName);
                    numberOfFile++;
                }
            }
        }
    }

    private File createFileAndSetPathToIterate() throws IOException, PathNotFoundException {
        consolePrinter.enterPathToCreateResultFile();
        String pathToNewTxtString = dataReader.loadCharacters();
        File pathToNewTxtFile = new File(pathToNewTxtString);

        resultFile = dataReader.createResultFile(pathToNewTxtFile);

        consolePrinter.enterPathToDirectories();
        String pathToDirectoriesString = dataReader.loadCharacters();
        pathToDirectoriesFile = new File(pathToDirectoriesString);
        dataReader.openBufferedWriter();

        return pathToDirectoriesFile;
    }
}
