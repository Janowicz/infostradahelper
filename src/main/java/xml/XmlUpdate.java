package main.java.xml;

import main.java.io.DataReader;
import main.java.io.ConsolePrinter;

import java.io.*;
import java.util.*;

public class XmlUpdate {

    private ConsolePrinter consolePrinter = new ConsolePrinter();
    private DataReader dataReader = new DataReader();
    private Map<String, String> dzialkaPrzedMap = new HashMap<>();
    private Map<String, String> dzialkaPoMap = new HashMap<>();
    private Set<String> operatSet = new HashSet<>();
    private String newTxt;
    private String line;
    private int dzialkaPrzedNumber = 0;
    private int dzialkaPoNumber = 0;
    private int z = 1;
    private BufferedReader reader;
    private File xmlPath;

    public void upDateDzialkaInXmlCase7() throws IOException {
        System.out.println("\nThis option add dzialkaPrzed and dzialkaPo to xml." +
                "If dzialkaPrzed or dzialkaPo is exists in xml, it will be override." +
                "\nYou need two txt file with: " +
                "- 'name xml files (without file extension), dzialkaPrzed' separated by TAB." +
                "- 'name xml files (without file extension), dzialkaPo' separated by TAB." +
                "\nFor example:" +
                "\nxml name           | dzialkaPrzed " +
                "\nP.0410.2017.1.xml  | 041005_5.0027.1/1,041005_5.0027.1/1" +
                "\n '|' means 'TAB', ',' is separator between each dzialkaPrzed or dzialkaPo"
        );

        consolePrinter.enterPathWithDzialkaPrzed();
        File dzialkaPrzedPath = new File(dataReader.loadCharacters());

        consolePrinter.enterPathWithDzialkaPo();
        File dzialkaPoPath = new File(dataReader.loadCharacters());

        consolePrinter.enterPathToXmlDirectory();
        xmlPath = new File(dataReader.loadCharacters());

        reader = dataReader.openFileBufferedReader(dzialkaPrzedPath);
        line = reader.readLine();
        addDzialkaToMap(line, dzialkaPrzedMap);
        reader.close();

        reader = dataReader.openFileBufferedReader(dzialkaPoPath);
        line = reader.readLine();
        addDzialkaToMap(line, dzialkaPoMap);
        reader.close();

        String[] xmlList = xmlPath.list();
        String[] xmlFiles = rewriteArrayWithoutExtension(xmlList);

        createAndWriteNewXmlFiles(xmlFiles);
        z = 1;
    }

    private void createAndWriteNewXmlFiles(String[] newXmlFilesArray) throws IOException {
        for (int i = 0; i < newXmlFilesArray.length; i++) {
            if (operatSet.contains(newXmlFilesArray[i])) {
                System.out.println(z++ + " " + newXmlFilesArray[i]);
                dzialkaPrzedNumber = 0;
                dzialkaPoNumber = 0;

                newTxt = newXmlFilesArray[i] + ".txt";
                File newTxtFile = new File(xmlPath + File.separator + newTxt);

                if (!newTxtFile.exists()) {
                    newTxtFile.createNewFile();
                }

                BufferedReader readerXml = dataReader.openFileBufferedReader(new File(xmlPath + File.separator + newXmlFilesArray[i] + ".xml"));
                String xmlLine = readerXml.readLine();

                BufferedWriter writerXml = dataReader.openBufferedWriterToNewFile(newTxtFile);
                while (xmlLine != null) {
                    if (xmlLine.contains("<dzialkaPrzed") && dzialkaPrzedNumber == 0) {
                        String dzialkaPrzedValue = dzialkaPrzedMap.get(newXmlFilesArray[i]);
                        if (dzialkaPrzedValue != null) {

                            if (dzialkaPrzedValue.contains(",")) {

                                String[] dzialkaPrzedSplit = dzialkaPrzedValue.split(",");

                                for (int i1 = 0; i1 < dzialkaPrzedSplit.length; i1++) {
                                    writerXml.write("<dzialkaPrzed>" + dzialkaPrzedSplit[i1] + "</dzialkaPrzed>");
                                    writerXml.newLine();
                                }
                            } else {
                                writerXml.write("<dzialkaPrzed>" + dzialkaPrzedValue + "</dzialkaPrzed>");
                                writerXml.newLine();
                            }
                        } else {
                            writerXml.write("<dzialkaPrzed></dzialkaPrzed>");
                            writerXml.newLine();
                        }
                        dzialkaPrzedNumber++;
                    } else if (xmlLine.contains("<dzialkaPrzed") && dzialkaPrzedNumber > 0) {

                    } else if (xmlLine.contains("<dzialkaPo") && dzialkaPoNumber == 0) {
                        String dzialkaPoValue = dzialkaPoMap.get(newXmlFilesArray[i]);
                        if (dzialkaPoValue != null) {

                            if (dzialkaPoValue.contains(",")) {

                                String[] dzialkaPoSplit = dzialkaPoValue.split(",");

                                for (int i1 = 0; i1 < dzialkaPoSplit.length; i1++) {
                                    writerXml.write("<dzialkaPo>" + dzialkaPoSplit[i1] + "</dzialkaPo>");
                                    writerXml.newLine();
                                }
                            } else {
                                writerXml.write("<dzialkaPo>" + dzialkaPoValue + "</dzialkaPo>");
                                writerXml.newLine();
                            }
                        } else {
                            writerXml.write("<dzialkaPo></dzialkaPo>");
                            writerXml.newLine();
                        }
                        dzialkaPoNumber++;
                    } else if (xmlLine.contains("<dzialkaPo") && dzialkaPoNumber > 0) {

                    } else {
                        writerXml.write(xmlLine);
                        writerXml.newLine();
                    }
                    xmlLine = readerXml.readLine();
                    writerXml.flush();
                }
                dataReader.closeBufferedWriter();
            }
        }
    }

    private String[] rewriteArrayWithoutExtension(String[] oldArray){
        String[] newXmlList = new String[oldArray.length];
        for (int i = 0; i < oldArray.length; i++) {
            String newName = oldArray[i].substring(0, oldArray[i].length() - 4);
            newXmlList[i] = newName;
        }
        return newXmlList;
    }

    private void addDzialkaToMap(String line, Map map) throws IOException {
        while (line != null) {
            StringTokenizer tokenizer = new StringTokenizer(line, "\t");
            String tokenOperat = tokenizer.nextToken();
            if (tokenizer.hasMoreTokens()) {
                String tokenDzialka = tokenizer.nextToken();
                map.put(tokenOperat, tokenDzialka);
                operatSet.add(tokenOperat);

            }
            line = reader.readLine();
        }
    }
}
