package main.java.io;

public class ConsolePrinter {

    public void printCopyrightSign() {
        System.out.println("\n\u00AE Mateusz Janowicz\n");
    }

    public void enterPathToCreateResultFile() {
        System.out.println("Enter path to create txt file with results.");
    }

    public void enterPathToCreateErrorFile() {
        System.out.println("Enter path to create txt file with errors.");
    }

    public void enterPathToDirectories() {
        System.out.println("Enter path to directories.");
    }

    public void enterPathToPdfFile(){
        System.out.println("Enter path to pdf file.");
    }

    public void enterPathToDirectoryWithFiles() {
        System.out.println("Enter path to directory with files.");
    }

    public void enterPathToSourceDirectories() {
        System.out.println("Enter path to source directories.");
    }

    public void enterPathToDestinationDirectories() {
        System.out.println("Enter path to destination directories.");
    }

    public void closeApplication() {
        System.out.println("The program is going down...");
    }

    public void enterPartTextPDFContains() {
        System.out.println("Enter part of text which PDF files should contain");
    }

    public void enterPartTextFileContains() {
        System.out.println("Enter part of text which file should contain");
    }

    public void enterPathWithDzialkaPrzed() {
        System.out.println("Enter path to txt file with dzialkaPrzed");
    }

    public void enterPathWithDzialkaPo() {
        System.out.println("Enter path to txt file with dzialkaPo");
    }

    public void enterPathFromWillBeCopiedFiles(){
        System.out.println("Enter path FROM will be copy files");
    }

    public void enterDestinationPath(){
        System.out.println("Enter destination path");
    }

    public void enterPathToTxtFileWithData() {
        System.out.println("\nEnter path to txt file with mapping");
    }

    public void enterPAthToTxtFileWithNamesFiles(){
        System.out.println("Enter path to txt file with files name");
    }

    public void enterPathToXmlDirectory() {
        System.out.println("Enter path to directory with xml. New files will be " +
                "save with '.txt' extension.");
    }

    public void renameXml(){
        System.out.println("Rename xml files by text betweent: " +
                "\n<pierwszyCzlon>1</pierwszyCzlon>" +
                "\n<drugiCzlon>2</drugiCzlon>" +
                "\n<trzeciCzlon>3</trzeciCzlon>" +
                "\n<czwartyCzlon>4</czwartyCzlon>" +
                "\nand add dot betweent text. Result: 1.2.3.4");
    }

    public void printInfoInCopyDirsToAnother() {
        System.out.println("Mapping source directory -> destination directory should be save in txt file by schema");
        System.out.println("sourceDirectoryName|destinationDirectoryName");
        System.out.println("'|' means separator by names");
    }

    public void printInfoRenameFilesToAdd_AndDirName(){
        System.out.println("Add dir name and '_' if files does not contains");
    }

    public void printInfoInCopyFilesToAnotherDirsByContainsName() {
        System.out.println("nProgram copy files from one directory to another directories or subdirectories.\n" +
                "If directory CONTAINS file name, file will be copy to this directory.");
    }

    public void printInfoInCopyFilesToAnotherDirsByBefore__(){
        System.out.println("nProgram copy files from one directory to another directories or subdirectories.\n" +
                "If directory name is equals file name starts text to char '_', file will be copy to this directory.\n" +
                "For example: file P.0410.1999.12_STR-TYT-001.PDF will be copy to directory P.0410.1999.12.");
    }

    public void setTitle(){
        System.out.println("Enter title");
    }

    public void setAuthor(){
        System.out.println("Enter author");
    }

    public void setSubject(){
        System.out.println("Enter suject");
    }

    public void setKeywords(){
        System.out.println("Enter keywords");
    }
}
