package main.java.io;

import main.java.exception.PathNotFoundException;

import java.io.*;

public class DataReader {

    BufferedReader reader;
    BufferedWriter writer;
    File newFile;

    public String loadCharacters() throws IOException {
        BufferedReader reader1 = new BufferedReader(new InputStreamReader(System.in));
        return reader1.readLine();
    }

    public BufferedReader openFileBufferedReader(File file) throws IOException {
        reader = new BufferedReader(new FileReader(file));
        return reader;
    }

    public void closeBufferedReader() throws IOException {
        reader.close();
    }

    public void openBufferedWriter() throws IOException {
        writer = new BufferedWriter(new FileWriter(newFile, true));
    }

    public void closeBufferedWriter() throws IOException {
        writer.close();
    }

    public BufferedWriter openBufferedWriterToNewFile(File file) throws IOException {
        writer = new BufferedWriter(new FileWriter(file));
        return writer;
    }

    public File createResultFile(File file) throws IOException, PathNotFoundException {
        checkIfFileExists(file);
        newFile = new File(file + File.separator + "result.txt");
        newFile.createNewFile();
        return newFile;
    }

    public void writeLineToTxtFile(String text) throws IOException {
        writer.write(text);
        writer.newLine();
        writer.flush();
    }

    public void writeTextToTxtFileWithoutPrintLine(String text) throws IOException {
        writer.write(text);
        writer.flush();
    }

    public File enterPath() throws IOException, PathNotFoundException {
        String pathString = loadCharacters();
        File pathFile = new File(pathString);

        checkIfFileExists(pathFile);
        return pathFile;
    }

    public boolean checkIfFileExists(File file) throws IOException, PathNotFoundException {

        boolean ifExists = file.exists();
        if (!ifExists) {
            throw new PathNotFoundException();
        }
        return ifExists;
    }
}


