package main.java.app;


import exception.WrongChoiceException;
import main.java.exception.PathNotFoundException;
import main.java.io.ConsolePrinter;

import java.io.IOException;

public class HelperApp {

    public static void main(String[] args) throws IOException, PathNotFoundException, WrongChoiceException {

        HelperControl helperControl = new HelperControl();
        ConsolePrinter consolePrinter = new ConsolePrinter();
        consolePrinter.printCopyrightSign();
        helperControl.controlLoop();
    }
}
