package main.java.app;

import exception.WrongChoiceException;

import main.java.direrctoryOperations.CopyDirs;
import main.java.direrctoryOperations.ListDirs;
import main.java.fileOperations.CopyFiles;
import main.java.fileOperations.RenameFiles;
import main.java.io.DataReader;
import main.java.exception.PathNotFoundException;
import main.java.io.ConsolePrinter;
import main.java.pdf.Pdf;
import main.java.xml.XmlGetData;
import main.java.xml.XmlUpdate;

import java.io.IOException;

public class HelperControl {

    private DataReader dataReader = new DataReader();
    private ConsolePrinter consolePrinter = new ConsolePrinter();
    private Pdf pdf = new Pdf();
    private XmlGetData xml = new XmlGetData();
    private XmlUpdate xmlUpdate = new XmlUpdate();
    private CopyDirs copyDirs = new CopyDirs();
    private ListDirs listDirs = new ListDirs();
    private CopyFiles copyFiles = new CopyFiles();
    private RenameFiles renameFiles = new RenameFiles();

    private static final String EXIT = "0";
    private static final String DEFECTIVE_PDF = "1";
    private static final String COUNT_ALL_PAGES = "2";
    private static final String COUNT_PAGES_BY_TEXT = "3";
    private static final String GET_PAGES_SIZE_PDF = "4";
    private static final String GET_DATA_FROM_XML = "5";
    private static final String GET_DATA_FROM_XML_BY_ENTER_TEXT = "6";
    private static final String ADD_DZIALKAPRZED_DZIALKAPO_TO_XML = "7";
    private static final String COPY_DIRS_TO_ANOTHER = "8";
    private static final String LIST_ALL_DIRS = "9";
    public static final String COPY_FILES_TO_DIRS_BY_CONTAINS_NAME = "10";
    public static final String COPY_FILES_TO_DIRS_BEFORE__ = "11";
    private static final String COPY_FILES_IF_TIF_HAS_THE_SAME_NAME = "12";
    private static final String RENAME_XML = "13";
    private static final String ADD_DIR_NAME_AND___IF_DOES_NOT_CONTAINS = "14";
    private static final String ADD_ATTBIBUTES_TO_PDF_FILE = "15";
    private static final String COPY_FILES_BY_TXT_FILE = "16";

    public void controlLoop() throws IOException, PathNotFoundException {

        while (true) {
            printOptions();
            String mainChoice = dataReader.loadCharacters();

            switch (mainChoice) {
                case DEFECTIVE_PDF:
                    try {
                        pdf.defectivePdfCase1();
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case COUNT_ALL_PAGES:
                    try {
                        pdf.countAllPagesCase2();
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case COUNT_PAGES_BY_TEXT:
                    try {
                        pdf.countPagesByTextCase3();
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case GET_PAGES_SIZE_PDF:
                    try {
                        pdf.getPageSizeCase4();
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case GET_DATA_FROM_XML:
                    try {
                        xml.getDataFromXml();
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case GET_DATA_FROM_XML_BY_ENTER_TEXT:
                    try {
                        xml.getDataFromFileFileByEnterText();
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case ADD_DZIALKAPRZED_DZIALKAPO_TO_XML:
                    try {
                        xmlUpdate.upDateDzialkaInXmlCase7();
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case COPY_DIRS_TO_ANOTHER:
                    try {
                        copyDirs.copyDirsToAnother();
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case LIST_ALL_DIRS:
                    try {
                        listDirs.listDirsToTxtFile();
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case COPY_FILES_TO_DIRS_BY_CONTAINS_NAME:
                    try {
                        copyFiles.copyFilesFromOneDirToAnother(COPY_FILES_TO_DIRS_BY_CONTAINS_NAME);
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case COPY_FILES_TO_DIRS_BEFORE__:
                    try {
                        copyFiles.copyFilesFromOneDirToAnother(COPY_FILES_TO_DIRS_BEFORE__);
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case COPY_FILES_IF_TIF_HAS_THE_SAME_NAME:
                    try {
                        copyFiles.copyFilesIfTifHasTheSameName();
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case RENAME_XML:
                    try {
                        renameFiles.renameXml();
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case ADD_DIR_NAME_AND___IF_DOES_NOT_CONTAINS:
                    try {
                        renameFiles.renameFilesAdd_AndDirName();
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case ADD_ATTBIBUTES_TO_PDF_FILE:
                    try {
                        pdf.addAttributes();
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case COPY_FILES_BY_TXT_FILE:
                    try {
                        copyFiles.copyFilesByTxtFile();
                    } catch (NullPointerException e) {
                        throw new PathNotFoundException();
                    } finally {
                        controlLoop();
                    }
                    break;
                case EXIT:
                    consolePrinter.closeApplication();
                    System.exit(0);
                    break;
                default:
                    System.err.println("Select a correct option!");
            }
        }
    }

    private void printOptions() {
        System.out.println(" _____________________________________________________________");
        System.out.println("| " + DEFECTIVE_PDF + " - check if PDF files in directories are defective         |");
        System.out.println("| " + COUNT_ALL_PAGES + " - count pages in all PDF files                            |");
        System.out.println("| " + COUNT_PAGES_BY_TEXT + " - count pages in PDF files by text in name files          |");
        System.out.println("| " + GET_PAGES_SIZE_PDF + " - return size of each page in PDF files                   |");
        System.out.println("| " + GET_DATA_FROM_XML + " - get data from xml [all text(all line) from all lines]   |");
        System.out.println("|    <obreb>, <pzg_oznMaterialuZasobu>, <pzg_opis>,           |");
        System.out.println("|    <pzg_cel>, <celArchiwalny>                               |");
        System.out.println("| " + GET_DATA_FROM_XML_BY_ENTER_TEXT + " - get data from xml by enter text                         |");
        System.out.println("| " + ADD_DZIALKAPRZED_DZIALKAPO_TO_XML + " - add <dzialkaPrzed> and/or <dzialkaPo> to xml            |");
        System.out.println("| " + COPY_DIRS_TO_ANOTHER + " - copy directories to another by mapping in txt file      |");
        System.out.println("| " + LIST_ALL_DIRS + " - list all directories and subdirectories to txt file     |");
        System.out.println("| " + COPY_FILES_TO_DIRS_BY_CONTAINS_NAME + " - copy files to directories if name of file              |");
        System.out.println("|      starts with name of directory                          |");
        System.out.println("| " + COPY_FILES_TO_DIRS_BEFORE__ + " - copy files to directory if directory name is equals    |");
        System.out.println("|       file name before char '_'                             |");
        System.out.println("| " + COPY_FILES_IF_TIF_HAS_THE_SAME_NAME + " - copy files to directory if tif file has                |");
        System.out.println("|       the same name                                         |");
        System.out.println("| " + RENAME_XML + " - rename xml                                             |");
        System.out.println("| " + ADD_DIR_NAME_AND___IF_DOES_NOT_CONTAINS + " - add dir name and '_' to files name if doesn't contains |");
        System.out.println("| " + ADD_ATTBIBUTES_TO_PDF_FILE + " - add attributes to pdf file                             |");
        System.out.println("| " + COPY_FILES_BY_TXT_FILE + " - copy files from many directories to one directory      |");
        System.out.println("|     by names saved in txt file                              |");
        System.out.println("| " + EXIT + " - exit                                                    |");
        System.out.println("|_____________________________________________________________|");
    }
}
